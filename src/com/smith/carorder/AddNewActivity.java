package com.smith.carorder;

import th.ac.tu.siit.lab7database.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class AddNewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);

		Intent d = this.getIntent();
		if (d.hasExtra("id")) {
			String name = d.getStringExtra("name");
			String phone = d.getStringExtra("phone");

			EditText etName = (EditText) findViewById(R.id.etName);
			EditText etPhone = (EditText) findViewById(R.id.etPhone);

			etName.setText(name);
			etPhone.setText(phone);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText) findViewById(R.id.etName);
			EditText etPhone = (EditText) findViewById(R.id.etPhone);
		

			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
		

			if (sName.length() == 0 || sPhone.length() == 0) {
				Toast t = Toast.makeText(this,
						"Contact name,  phone are required",
						Toast.LENGTH_LONG);
				t.show();
			} else {
				Intent d = this.getIntent();
				Intent data = new Intent();

				data.putExtra("name", sName);
				data.putExtra("phone", sPhone);
				data.putExtra("id", d.getLongExtra("id", 0));
				
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
