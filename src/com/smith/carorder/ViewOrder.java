package com.smith.carorder;

import th.ac.tu.siit.lab7database.R;
import android.os.Bundle;
import android.app.ListActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.widget.SimpleCursorAdapter;

public class ViewOrder extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor; // Manage the
	SimpleCursorAdapter adapter;
	Long cid; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Intent i = this.getIntent();
		cid = i.getLongExtra("cid", 0);

		
		
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		cursor = getAllorder();
		adapter = new SimpleCursorAdapter(this, R.layout.order, cursor,
				new String[] { "_id", "ct_name", "car_name" }, new int[] {
						R.id.orderid, R.id.cid, R.id.carname}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
		
	}
	
	protected Cursor getAllorder() {
		// db.query = execute a SELECT statement and return a cursor
		return db.rawQuery("SELECT orders._id, contacts.ct_name, orders.car_name " +
				" FROM orders, contacts" +
				" WHERE contacts._id = orders.c_id and orders.c_id = "+ cid +
				" GROUP BY orders._id", null);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view, menu);
		return true;
	}

}
