package com.smith.carorder;

import th.ac.tu.siit.lab7database.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;



public class ProductActivity extends Activity implements OnClickListener {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor; 
	SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product);
		
		Button b1 = (Button)findViewById(R.id.button1);
		
		b1.setOnClickListener(this);
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.product, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		EditText e1 = (EditText)findViewById(R.id.editText1);
		
		if (e1.length() == 0) {
			Toast t = Toast.makeText(this,
					"Please Specified car name",
					Toast.LENGTH_LONG);
			t.show();
		} else {
		
			String car = e1.getText().toString();
			Intent d = this.getIntent();
			Intent data = new Intent();
			data.putExtra("id", d.getLongExtra("id", 0));
			data.putExtra("car_name", car);
			
			setResult(RESULT_OK, data);
			finish();
		}
		
	}
	
	

}
