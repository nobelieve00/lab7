package com.smith.carorder;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DBHelper extends SQLiteOpenHelper {

	private static final String DBNAME = "contacts.db";
	private static final int DBVERSION = 14;

	public static final String sql = "CREATE TABLE contacts ("
			+ "_id integer primary key autoincrement, "
			+ "ct_name text not null, " + "ct_phone text not null);";

	public static final String sql2 = "CREATE TABLE orders ("
			+ "_id integer primary key autoincrement, "
			+ "c_id text not null, " + "car_name text not null);";

	public DBHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		
		db.execSQL(sql);

		db.execSQL(sql2);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS contacts");
        db.execSQL("DROP TABLE IF EXISTS orders");
		this.onCreate(db);
	}

}
